import React from 'react';
import Form from './components/Form';
import './App.css';
import Nav from './components/Nav';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import About from './components/About'
import LandingPage from './components/LandingPage'

class App extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <>

        <Routes>
          <Route path="/about" element={<About />} exact />
          <Route path="/" element={<Form />} exact />
          <Route path="/pages" element={<LandingPage />} exact />
        </Routes>

      </>
    );
  };
};

export default App;
