import React from 'react';

class Footer extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <footer>

                <span>If you have any queries, please send an email with your question to abc@email.com </span>
            </footer>
        )
    }
}

export default Footer;