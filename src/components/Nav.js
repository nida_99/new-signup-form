import React from 'react';
import { Link } from 'react-router-dom'
import './pages.css'

class Nav extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <nav className="nav">
                <Link to='/about' className='link'>ABOUT US</Link>
                <Link to='/' className='link'>SIGNUP</Link>
                <Link to='/pages' className='link'>PAGES</Link>
            </nav>
        );
    };
};

export default Nav;