import React from 'react';
import '../App.css'
import validator from 'validator';
import Image from './image/Sign-up.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css'
import Nav from './Nav';

class Form extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            userName: "",
            userAge: "",
            userEmail: "",
            userPassword: "",
            confirmPassword: "",
            checkbtn: false,
            currentStatus: " ",
            setClassName: "d-none",
            errors: {},
        };
    };
    handleChanges = (event) => {


        let value = event.target.value;

        if (event.target.type === "checkbox") {
            value = event.target.checked;
        }

        this.setState({
            [event.target.id]: value
        });

    };

    formValidation = () => {

        const { userName, userAge, userEmail, userPassword, confirmPassword, checkbtn } = this.state;
        const error = {};

        let isValid = true;

        if (validator.isEmpty(userName)) {
            error.name = "Please enter your name.";
            isValid = false;
        }

        else if (!validator.isLength(userName, { min: 3, max: undefined }) ||
            !validator.isAlpha(userName, 'en-US', { ignore: " -" })) {
            error.name = "Name must be 3 characters or more.";
            isValid = false;
        }

        if (validator.isEmpty(userAge)) {
            error.age = "Please enter your age.";
        }

        else if (validator.isInt(userAge, { min: 19, max: 99 }) === false) {
            error.age = "Age must be greater than 18 and less than 100.";
            isValid = false;
        }

        if (!validator.isEmail(userEmail)) {
            error.userEmail = "Please enter valid Email-id."
            isValid = false;
        };

        if (validator.isEmpty(userPassword)) {
            error.password = "Please enter your password.";
            isValid = false;
        }

        else if (validator.isStrongPassword(userPassword, { minLength: 8, minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1 }) === false) {
            error.password = "Password:Min 8 char,1 lowecase,1 uppercase,1 symbol & 1 No."
            isValid = false;
        };

        if (validator.isEmpty(confirmPassword)) {
            error.confirmPassword = "Please enter your password again.";
            isValid = false;
        };

        if (validator.equals(confirmPassword, userPassword) === false) {
            error.confirmPassword = "Password is not same. Please try again."
            isValid = false;
        };

        if (checkbtn === false) {
            error.checkboxValue = "Click the checkbox button to confirm agreement."
            isValid = false;
        };

        this.setState({
            errors: error
        });

        return isValid;

    };

    handleSubmit = (event) => {
        event.preventDefault();
        const valid = this.formValidation();


        if (valid) {
            this.setState({
                currentStatus: "Congratulations, Form submitted!",
                setClassName: "alert alert-success p-3",
                userName: "",
                userAge: "",
                userEmail: "",
                userPassword: "",
                confirmPassword: "",
                checkbtn: false,
            })

        } else {
            this.setState({
                currentStatus: "",
                setClassName: "d-none"
            });
        };
    };

    render() {

        return (
            <>
                <Nav />
                <section className={this.state.setClassName} >
                    {this.state.currentStatus}
                </section>
                <section className='Form my-5'>
                    {/* <section className='Form my-5 bg-dark'> */}
                    <div className='container my-5 pt-6 d-flex justify-content-center .justify-content-lg-start mt-5 pt-5 border border-dark-2'>

                        <div className="col-lg-4 col-md-6 d-none d-lg-block d-flex mt-3 mb-5 mx-4 align-items-center .align-items-md-center" >
                            <img className="img-responsive img-fluid img-thumbnail" src={Image} /> </div>
                        <div className='col-lg-5 col-md-7 px-4  mb-4' >
                            <form>
                                <h1 className='font-monospace fs-1 fw-bolder text-center'>BARREL</h1>
                                <h1 className='text-center'>Sign up</h1>

                                <div className='row '>

                                    <input className='my-2 p-1.5' type="text"
                                        id="userName"
                                        placeholder="Enter Name"
                                        value={this.state.userName}
                                        onChange={this.handleChanges} />
                                </div>
                                <div
                                    className={this.state.errors.name ? 'alert-danger my-0.5 border-danger .text-wrap ' : 'no-status'}>
                                    {this.state.errors.name}
                                </div>



                                <div className='row'>
                                    <input className='my-2 p-1.5' type="number"
                                        id="userAge"
                                        placeholder="Enter Age"
                                        value={this.state.userAge}
                                        onChange={this.handleChanges} />
                                </div>
                                <div
                                    className={this.state.errors.age ? 'alert-danger my-0.5 border-danger .text-wrap ' : 'no-status'}>
                                    {this.state.errors.age}
                                </div>

                                <div className='row'>
                                    <input className='my-2 p-1.5' type="email"
                                        id="userEmail"
                                        placeholder="Enter Email-id"
                                        value={this.state.userEmail}
                                        onChange={this.handleChanges} />
                                </div>
                                <div
                                    className={this.state.errors.userEmail ? 'alert-danger my-0.5border-danger .text-wrap ' : 'no-status'}>
                                    {this.state.errors.userEmail}
                                </div>

                                <div className='row'>
                                    <input className='my-2 p-1.5' type="password"
                                        id="userPassword"
                                        placeholder="Enter Password"
                                        value={this.state.userPassword}
                                        onChange={this.handleChanges} />
                                </div>
                                <div
                                    className={this.state.errors.password ? 'alert-danger my-0.5 border-danger .text-wrap ' : 'no-status'}>
                                    {this.state.errors.password}
                                </div>

                                <div className='row'>
                                    <input className='my-2 p-1.5' type="password"
                                        id="confirmPassword"
                                        placeholder="Confirm Password"
                                        value={this.state.confirmPassword}
                                        onChange={this.handleChanges} />
                                </div>
                                <div
                                    className={this.state.errors.confirmPassword ? 'alert-danger my-0.5 border-danger .text-wrap ' : 'no-status'}>
                                    {this.state.errors.confirmPassword}
                                </div>

                                <div
                                    className='row'>
                                    <input className="my-2 p-1.5" type='checkbox'
                                        id="checkbtn"
                                        onClick={this.handleChanges} />

                                    <span className="text-center"> I have read and agree to the term of services</span>

                                    <div
                                        className={this.state.errors.checkboxValue ? 'alert-danger my-0.5 border-danger .text-wrap ' : 'no-status'}>
                                        {this.state.errors.checkboxValue}
                                    </div>
                                </div>




                                <div className='row'>
                                    <button onClick={this.handleSubmit} className='btn-primary .text-wrap  my-3 p-1.5'>
                                        Sign up
                                    </button>
                                </div>

                            </form>
                        </div>

                    </div>
                </section>

            </>
        );
    };
};

export default Form;