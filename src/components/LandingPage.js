import React from 'react';
import Footer from './Footer';
import Nav from './Nav';


class LandingPage extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <Nav />
                <div className='main-box'>
                    <section className='heading-content'>
                        <h1>Reasons to sign-up</h1>
                        <p>  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                            Et aspernatur impedit enim a maiores! Ut voluptates
                            pariatur dolorum voluptate nesciunt accusantium saepe
                        </p>
                        <button>Know more</button>
                    </section>
                    <div className='box-2'>
                        LOREM IPSUM
                    </div>
                </div>
                <div className='box-3'>

                    <section className='listing-item'>
                        <h2> What makes you stand out?</h2>
                        <ul>
                            <li> pariatur dolorum voluptate nesciunt</li>
                            <li> pariatur dolorum voluptate nesciunt</li>
                            <li> pariatur dolorum voluptate nesciunt</li>
                            <li> pariatur dolorum voluptate nesciunt</li>
                            <li> pariatur dolorum voluptate nesciunt</li>
                            <li> pariatur dolorum voluptate nesciunt</li>

                        </ul>
                    </section>
                </div>

                <Footer />
            </>
        );
    };
};

export default LandingPage;